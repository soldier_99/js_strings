const realNo =strArr=>{

  newArr = [] ;

  for(let i = 0;i<strArr.length ; i++){
    let s = strArr[i].replace(/[$,]/g, "");
    
    isNaN(s) ? newArr.push(0) : newArr.push(parseFloat(s)) ;
  }
  return newArr ; 
}
    
module.exports = realNo ;